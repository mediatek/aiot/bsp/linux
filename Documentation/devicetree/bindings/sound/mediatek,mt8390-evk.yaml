# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/sound/mediatek,mt8390-evk.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: MediaTek MT8188 ASoC sound card

maintainers:
  - Parker Yang <parker.yang@mediatek.com>

properties:
  compatible:
    const: mediatek,mt8390-evk

  model:
    $ref: /schemas/types.yaml#/definitions/string
    description: User specified audio sound card name

  audio-routing:
    $ref: /schemas/types.yaml#/definitions/non-unique-string-array
    description:
      A list of the connections between audio components. Each entry is a
      sink/source pair of strings. Valid names could be the input or output
      widgets of audio components, power supplies, MicBias of codec and the
      software switch.

  mediatek,platform:
    $ref: /schemas/types.yaml#/definitions/phandle
    description: The phandle of MT8188 ASoC platform.

patternProperties:
  "^dai-link-[0-9]+$":
    type: object
    description: |
      Array of dai-links with their corresponding codecs.
    properties:
      sound-dai:
          $ref: "/schemas/types.yaml#/definitions/phandle"
          description: |
            The phandle of the MT8188 ASoC platform.
          maxItems: 1
      dai-link-name:
        description: |
          This property corresponds to the name of the dai-link to which we are
          going to attached the codecs present in this node.
        maxItems: 1
    patternProperties:
      "^codec-[0-9]+$":
        type: object
        description: |
          Array of nodes containing information about a codec to be attached to
          the given dai-link.
        properties:
          sound-dai:
            $ref: /schemas/types.yaml#/definitions/phandle-array
            description: |
              The phandle of the audio codec and the driver id when needed.
            maxItems: 1
    required:
      - dai-link-name

additionalProperties: false

required:
  - compatible
  - mediatek,platform

examples:
  - |

    sound: mt8188-sound {
        compatible = "mediatek,mt8390-evk";
        mediatek,platform = <&afe>;
        model = "mt8188_evk";
        pinctrl-names = "default";
        pinctrl-0 = <&aud_pins_default>;
        audio-routing =
            "Headphone", "Headphone L",
            "Headphone", "Headphone R",
            "AIN1", "Headset Mic",
            "DMIC_INPUT", "AP DMIC",
            "AP DMIC", "MIC_BIAS_0",
            "AP DMIC", "MIC_BIAS_2";

        dai-link-0 {
            sound-dai = <&afe>;
            dai-link-name = "ADDA_BE";

            codec-0 {
                sound-dai = <&pmic 0>;
            };
        };

        dai-link-1 {
            sound-dai = <&afe>;
            dai-link-name = "ETDM3_OUT_BE";

            codec-0 {
                sound-dai = <&hdmi0>;
            };
        };
    };

...
