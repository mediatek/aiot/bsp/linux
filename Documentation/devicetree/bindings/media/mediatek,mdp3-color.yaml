# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/media/mediatek,mdp3-color.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: MediaTek Media Data Path 3 COLOR Device Tree Bindings

maintainers:
  - Matthias Brugger <matthias.bgg@gmail.com>
  - Moudy Ho <moudy.ho@mediatek.com>

description: |
  One of Media Data Path 3 (MDP3) components used to adjust hue, luma and
  saturation to get better picture quality.

properties:
  compatible:
    enum:
      - mediatek,mt8195-mdp3-color

  reg:
    maxItems: 1

  mediatek,gce-client-reg:
    $ref: /schemas/types.yaml#/definitions/phandle-array
    items:
      items:
        - description: phandle of GCE
        - description: GCE subsys id
        - description: register offset
        - description: register size
    description: The register of client driver can be configured by gce with
      4 arguments defined in this property. Each GCE subsys id is mapping to
      a client defined in the header include/dt-bindings/gce/<chip>-gce.h.

  clocks:
    minItems: 1

  power-domains:
    maxItems: 1

required:
  - compatible
  - reg
  - mediatek,gce-client-reg
  - clocks
  - power-domains

additionalProperties: false

examples:
  - |
    #include <dt-bindings/clock/mt8195-clk.h>
    #include <dt-bindings/gce/mt8195-gce.h>
    #include <dt-bindings/power/mt8195-power.h>

    mdp3-color0@14008000 {
      compatible = "mediatek,mt8195-mdp3-color";
      reg = <0x14008000 0x1000>;
      mediatek,gce-client-reg = <&gce1 SUBSYS_1400XXXX 0x8000 0x1000>;
      clocks = <&vppsys0 CLK_VPP0_MDP_COLOR>;
      power-domains = <&spm MT8195_POWER_DOMAIN_VPPSYS0>;
    };
